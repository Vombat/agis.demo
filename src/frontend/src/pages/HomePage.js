import React from 'react'
import title from '../data/title'


export const HomePage = () => {
    return(
        <div className='container'>
            <header className='center-align'>
                <h1>{title.Main}</h1>
                <h2>{title.Subtitle}</h2>
                <h3>{title.Home}</h3>
            </header>
        </div>
    )
}
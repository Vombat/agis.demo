import React, { useCallback, useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'
import { useHttp } from '../hooks/http.hook'
import { Loader } from '../components/Loader'
import { UserDetailCard } from '../components/UserDetailCard'

export const UserDetailPage = () => {
  const {request, loading} = useHttp()
  const [user, setUser] = useState(null)
  const userId = useParams().id

  const fetchUser = useCallback(async () => {
    try {
      const fetched = await request(`/api/users/${userId}`, 'GET', null)
      setUser(fetched)
    } catch (e) {}
  }, [userId, request])

  useEffect(() => {
    fetchUser()
  }, [fetchUser])

  if (loading) {
    return <Loader />
  }

  return (
    <>
      { !loading && user && <UserDetailCard user={ user } /> }
    </>
  )
}
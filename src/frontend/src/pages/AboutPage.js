import React from 'react'
import title from '../data/title'

export const AboutPage = () => {
    return(
        <div className='container'>
            <header className='center-align'>
                <a href="https://agis.kz/" target="_blank" rel="noreferrer"><h1>{title.Main}</h1></a>
                <h2>{title.Subtitle}</h2>
                <h3>{title.About}</h3>
            </header>
        </div>
    )
}
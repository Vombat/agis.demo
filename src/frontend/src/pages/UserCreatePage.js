import React, { useState, useEffect } from 'react'
import { UserCreateOrUpdateCard } from '../components/UserCreateOrUpdateCard'

export const UserCreatePage = () => {
  const [user, setUser] = useState({})

  useEffect(() => {
    setUser({
      _id: '',
      name: '',
      email: '',
      phone: ''
    })
  }, [setUser])

  return (
    <>
      { <UserCreateOrUpdateCard user={ user } /> }
    </>
  )
}
import React, { useCallback, useEffect, useState } from 'react'
import { useHttp } from '../hooks/http.hook'
import { Loader } from '../components/Loader'
import { UsersList } from '../components/UsersList'
import { Redirect } from "react-router-dom"

const title = 'AGIS';
const subtitle = 'Users - TestPage';

export const UsersPage = () => {
    const [users, setUsers] = useState([])
    const {request, loading} = useHttp()
    const [redirect, setRedirect] = useState(false)

    const fetchUsers = useCallback(async () => {
        try {
            const fetched = await request(`/api/users`, 'GET', null)
            setUsers(fetched)
        } catch (e) {}
      }, [request])

    const createHandler = async () => {
        setRedirect(true)
    }

    useEffect(() => {
    fetchUsers()
    }, [fetchUsers])

    if (loading) {
    return <Loader />
    }

    if (redirect) {
        return <Redirect to={'/users/create'} />
    }

    return(
        <div className="col ">
            <h2 className="header center-align">{title}</h2>
            <h2 className="header center-align">{subtitle}</h2>
            <div className="card horizontal">
                <div className="card-stacked">
                    <div className="card-action right-align">
                        <button className="right-align" onClick={createHandler}><i className="material-icons">person_add</i></button>
                    </div>
                    <div className="card-content">
                        {!loading && <UsersList users={ users } />}
                    </div>
                </div>
            </div>
        </div>
    )
}
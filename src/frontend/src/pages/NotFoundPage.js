import React from 'react'
const title = 'Not found Page';
export const NotFoundPage = () => {
    return(
        <div className='container'>
            <h1 className='center-align'>{title}</h1>
        </div>
    )
}
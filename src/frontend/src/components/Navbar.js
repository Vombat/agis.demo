import React, { useEffect } from 'react'
import M from 'materialize-css';
import { NavLink } from 'react-router-dom'
import { Url } from '../routes/Url'

export const Navbar = () => {

  const title = 'AGIS';

  useEffect( () => {
    let dropdowns = document.querySelectorAll('.dropdown-trigger');
    let options = {
        inDuration: 300,
        outDuration: 300,
        hover: true,
        coverTrigger: false,
        constrainWidth: false
    };

    M.Dropdown.init(dropdowns, options);
  })

  return (
    <nav>
      <div className="nav-wrapper blue darken-1" style={{ padding: '0 2rem' }}>
        <span className="brand-logo"><NavLink to={Url.home} >{title}</NavLink></span>
        <ul id="nav-mobile" className="right hide-on-med-and-down">
          <li>
            <a className="dropdown-trigger" href="#!" data-target="dropdownOrder"  style={{marginRight: 25}}><i className="material-icons">people</i></a>
            <ul id='dropdownOrder' className='dropdown-content'>
                    <li><NavLink to={Url.users}>Users</NavLink></li>
            </ul>
          </li>
          <li>
            <a className="dropdown-trigger" href="#!" data-target="dropdownProfile" style={{marginRight: 25}}><i className="material-icons">settings</i></a>
            <ul id='dropdownProfile' className='dropdown-content'>
              <li><NavLink to={Url.about}>About</NavLink></li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
  )
}

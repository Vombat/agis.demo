import React, { useEffect, useState } from 'react'
import { useHttp } from '../hooks/http.hook'
import { Redirect } from "react-router-dom"

export const UserCreateOrUpdateCard = ({ user }) => {
    const title = user._id ? 'Обновление пользователя' : 'Создание пользователя'
    const { request } = useHttp()
    const [form, setForm] = useState({
        name: user.name, email: user.email, phone: user.phone
    })
    const [userId, setUserId] = useState('')
    const [redirect, setRedirect] = useState(false)

    const changeHandler = event => {
        setForm({...form, [event.target.name]: event.target.value })
    }

    const saveHandler = async () => {
        const body = {
            name: form.name,
            email: form.email,
            phone: form.phone
        }
        user._id ? await request(`/api/users/${user._id}`, 'PUT', body) : await request(`/api/users`, 'POST', body)
        setRedirect(true)
    }

    useEffect(() => {
        setUserId(user._id)
    },[setUserId, user._id])

    if (redirect) {
        return <Redirect to={'/users'} />
    }

    return (
        <div className="col s12 m7">
            <h2 className="header center-align">{title}</h2>
            <div className="card horizontal">
            <div className="card-stacked">
                    <div className="card-action right-align">
                        <button className="right-align" onClick={saveHandler}><i className="material-icons">save</i></button>
                    </div>
                <div className="card-content">
                    <div className="row">
                        <form className="col s12">
                            <div className="row">
                                <div className="input-field col s10 offset-s1">
                                    <input disabled value={userId} id="disabled" type="text" className="validate"/>
                                </div>
                            </div>
                            <div className="row">
                                <div className="input-field col s10 offset-s1">
                                    <i className="material-icons prefix">account_circle</i>
                                    <input
                                        id="icon_name"
                                        type="text"
                                        className="validate"
                                        value={form.name}
                                        name='name'
                                        onChange={changeHandler}
                                    />
                                    <label htmlFor="icon_name">Name</label>
                                </div>
                            </div>
                            <div className="row">
                                <div className="input-field col s10 offset-s1">
                                    <i className="material-icons prefix">email</i>
                                    <input
                                        id="icon_email"
                                        type="email"
                                        className="validate"
                                        value={form.email}
                                        name='email'
                                        onChange={changeHandler}/>
                                    <label htmlFor="icon_email">Email</label>
                                </div>
                            </div>
                            <div className="row">
                                <div className="input-field col s10 offset-s1">
                                    <i className="material-icons prefix">phone</i>
                                    <input
                                        id="icon_telephone"
                                        type="tel"
                                        className="validate"
                                        value={form.phone}
                                        name='phone'
                                        onChange={changeHandler}/>
                                    <label htmlFor="icon_telephone">Telephone</label>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            </div>
        </div>
    )
}
import React, { useState } from 'react'
import { Redirect } from "react-router-dom"
import { useHttp } from '../hooks/http.hook'



export const UserDeleteCard = ({ user }) => {
    const title = 'Удаление пользователя'
    const { request } = useHttp()
    const [redirect, setRedirect] = useState(false)

    const deleteHandler = async () => {
        await request(`/api/users/${user._id}`, 'DELETE', null)
        setRedirect(true)
    }

    if (redirect) {
        return <Redirect to={'/users'} />
    }

    return (
        <div className="col s12 m7">
            <h2 className="header center-align">{title}</h2>
            <div className="card horizontal">
            <div className="card-stacked">
                <div className="card-action right-align">
                    <button className="right-align" onClick={deleteHandler}><i className="material-icons">delete</i></button>
                </div>
                <div className="card-content">
                    <p><strong>ID:</strong> {user._id}</p>
                    <p><strong>Name:</strong> {user.name}</p>
                    <p><strong>Email:</strong> {user.email}</p>
                    <p><strong>Phone:</strong> {user.phone}</p>
                </div>
            </div>
            </div>
        </div>
    )
}
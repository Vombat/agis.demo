import React from 'react'

export const UserDetailCard = ({ user }) => {
    const title = 'Детализация по пользователю'
    return (
        <div class="col s12 m7">
            <h2 className="header center-align">{title}</h2>
            <div className="card horizontal">
            <div className="card-stacked">
                <div className="card-content">
                    <p><strong>ID:</strong> {user._id}</p>
                    <p><strong>Name:</strong> {user.name}</p>
                    <p><strong>Email:</strong> {user.email}</p>
                    <p><strong>Phone:</strong> {user.phone}</p>
                </div>
            </div>
            </div>
        </div>
    )
}
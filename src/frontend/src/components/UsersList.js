import React from 'react'

export const UsersList = ({ users }) => {
  if (!users.length) {
    return <p className="center">Список пользователей недоступен</p>
  }

  return (
    <table>
        <thead>
        <tr>
            <th>№</th>
            <th>ID</th>
            <th>Name</th>
            <th>Email</th>
            <th>Phone</th>
            <th>...</th>
        </tr>
        </thead>

        <tbody>
        { users.map((user, index) => {
            return (
            <tr key={user._id}>
                <td>{index + 1}</td>
                <td>{user._id}</td>
                <td>{user.name}</td>
                <td>{user.email}</td>
                <td>{user.phone}</td>
                <td>
                    <a href={`/users/detail/${user._id}`} ><i className="material-icons">description</i></a>
                    <a href={`/users/update/${user._id}`} ><i className="material-icons">edit</i></a>
                    <a href={`/users/delete/${user._id}`} ><i className="material-icons">delete</i></a>
                </td>
            </tr>
            )
        }) }
        </tbody>
    </table>
  )
}

export const Url = {
    home: '/',
    about: '/about',
    users: '/users',
    userDetail: '/users/detail/:id',
    userCreate: '/users/create',
    userUpdate: '/users/update/:id',
    userDelete: '/users/delete/:id',
    notfound: '/404',
}
import React from 'react'
import {Switch, Route, Redirect} from 'react-router-dom'
import { HomePage } from '../pages/HomePage'
import { UsersPage } from '../pages/UsersPage'
import { UserDetailPage } from '../pages/UserDetailPage'
import { UserUpdatePage } from '../pages/UserUpdatePage'
import { UserDeletePage } from '../pages/UserDeletePage'
import { UserCreatePage } from '../pages/UserCreatePage'
import { AboutPage } from '../pages/AboutPage'
import { NotFoundPage } from '../pages/NotFoundPage'
import { Url } from './Url'

export const useRoutes = () => {
    return (
      <Switch>
        <Route path={Url.home} exact>
          <HomePage />
        </Route>
        <Route path={Url.users} exact>
          <UsersPage />
        </Route>
        <Route path={Url.userDetail} exact>
          <UserDetailPage />
        </Route>
        <Route path={Url.userDelete} exact>
          <UserDeletePage />
        </Route>
        <Route path={Url.userUpdate} exact>
          <UserUpdatePage />
        </Route>
        <Route path={Url.userCreate} exact>
          <UserCreatePage />
        </Route>
        <Route path={Url.about} exact>
          <AboutPage />
        </Route>
        <Route path={Url.notfound}>
          <NotFoundPage />
        </Route>
        <Redirect to={Url.notfound} />
      </Switch>
    )
}

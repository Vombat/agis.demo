const config = require('config')

const PORT = config.get('port') || config.util.getEnv('PORT_SERVER')

const message = {
    start: `App has been started on port ${PORT}...`,
    error: 'Что то пошло не так...',
    welcome: 'Welcome to Agis Demo Project'
}

module.exports = message
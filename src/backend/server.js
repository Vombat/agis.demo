const express = require('express')
const config = require('config')
const path = require('path')
const mongoose = require('mongoose')
const fetch = require('node-fetch');
const User = require('./models/User')
const message = require('./data/message')
const cors = require('cors')


const PORT = config.get('port') || config.util.getEnv('PORT_SERVER')


const app = express()
app.use(cors())
app.options('*', cors())

app.use('/', express.static(__dirname + '/'));
app.use(express.json({ extended: true }))

app.use('/api/users', require('./routes/user.routes'))
app.use('/', require('./routes/home.routes'))

mongoose
  .connect(config.get('mongoUri'), {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true
  })
  .then(async () => {
    console.log('MongoDB connected!');
    const dropDB = await mongoose.connection.db.dropDatabase();
    const message = dropDB
      ? 'DB is dropped before use'
      : 'Error drop DB before use';
    console.log(message);
  })
  .catch(error => console.log(error));

app.listen(PORT, () => console.log(message.start))


const getInitialUsersCollection = async () => {
  try {
    const response = await fetch('https://jsonplaceholder.typicode.com/users')
    const data = await response.json()

    const users = [...data].map( user => ({
      name: user.name,
      email: user.email,
      phone: user.phone,
    }))

  User.insertMany(users, (err, users) => (err) ? console.log(err) : console.log(users));
  } catch (error) {
    console.log(error)
  }
}

getInitialUsersCollection()


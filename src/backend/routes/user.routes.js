const { Router } = require('express')
const router = Router({ mergeParams: true })
const asyncHandler = require('express-async-handler');
const { StatusCodes } = require('http-status-codes');
const User = require('../models/User')

const getAll = async () => User.find({})

const getById = async id => User.findById(id)

const create = async entityToCreate => User.create(entityToCreate)

const update = async (id, entityToUpdate) => User.findByIdAndUpdate(id, entityToUpdate, { new: true })

const remove = async id => User.findByIdAndDelete(id)

router.get(
    '/',
    asyncHandler(async (req, res, next) => {
      const entities = await getAll();
      res.status(StatusCodes.OK).send(entities);
      next();
    })
  );

  router.get(
    '/:id',
    asyncHandler(async (req, res, next) => {
      const entity = await getById(req.params.id);
      entity
        ? res.status(StatusCodes.OK).send(entity)
        : res.sendStatus(StatusCodes.NOT_FOUND);
      next();
    })
  );

  router.delete(
    '/:id',
    asyncHandler(async (req, res, next) => {
      await remove(req.params.id);
      res.sendStatus(StatusCodes.NO_CONTENT);
      next();
    })
  );

  router.post(
    '/',
    asyncHandler(async (req, res, next) => {
      const entity = {...req.body};
      const newEntity = await create(entity);
      res.status(StatusCodes.OK).send(newEntity);
      next();
    })
  );

  router.put(
    '/:id',
    asyncHandler(async (req, res, next) => {
      const id = req.params.id;
      const entityToUpdate = {...req.body};
      await update(id, entityToUpdate);
      const entity = await getById(id);
      entity
        ? res.status(StatusCodes.OK).send(entity)
        : res.sendStatus(StatusCodes.NOT_FOUND);
      next();
    })
  );

module.exports = router
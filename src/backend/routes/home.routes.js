const {Router} = require('express')
const router = Router()
const message = require('../data/message')
const { StatusCodes } = require('http-status-codes');

router.get('/', async (req, res) => {
    try {
        return res.status(StatusCodes.OK).send(message.welcome)
    } catch (error) {
        res.status(StatusCodes.INTERNAL_SERVER_ERROR).json({message: message.error})
    }
})

module.exports = router